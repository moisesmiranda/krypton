﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Krypton.Tests
{
    
    public class EncryptTests
    {
        [Fact]
        public void TestandoCriptografia()
        {
            Xunit.Assert.Equal("8xlZjk+RmkFgX2a8tAqR8A==", Krypton.Encrypt("MOISESMIRANDA", true, "invent"));

        }

        [Fact]
        public void TesteDescriptografia()
        {
            Assert.Equal("MOISESMIRANDA", Krypton.Decrypt("8xlZjk+RmkFgX2a8tAqR8A==", true, "invent", false));
        }
    }
}
