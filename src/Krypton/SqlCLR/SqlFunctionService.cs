﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using Krypton.SqlCLR;
using System.Data.SqlClient;
using System.Collections;

namespace Krypton.SqlCLR
{

    /// <summary>
    /// Provê métodos de geração de funções no banco de dados
    /// </summary>
    public class SqlFunctionService
    {
        /// <summary>
        /// Construção de inicialização de objetos
        /// </summary>
        /// <param name="sqlConnection">Conexão com </param>
        public SqlFunctionService(SqlConnection sqlConnection)
        {
            this.SqlConnection = sqlConnection;
        }

        /// <summary>
        /// Cria funções que criptografia e descriptografia
        /// </summary>
        public void CreateFunctions()
        {

            var functionsNames = new String[] { "Krypton.SqlCLR.Encrypt.sql", "Krypton.SqlCLR.Decrypt.sql" };

            foreach (var functionName in functionsNames)
            {

                if (ExistFunction(functionName.Split('.')[2]))
                {
                    DropFunction(functionName.Split('.')[2]);
                    CreateFunction(functionName);
                }
                else
                {
                    CreateFunction(functionName);
                }
            }
        }

        /// <summary>
        /// Cria função de criptografia no banco de dados
        /// </summary>
        public void CreateEncryptFunction()
        {

            var functionName = "Krypton.SqlCLR.Encrypt.sql";

            if (ExistFunction(functionName.Split('.')[2]))
            {
                DropFunction(functionName.Split('.')[2]);
                CreateFunction(functionName);
            }
            else
            {
                CreateFunction(functionName);
            }

        }

        /// <summary>
        /// Cria função de descriptografia no banco de dados
        /// </summary>
        public void CreateDecryptFunction()
        {

            var functionName = "Krypton.SqlCLR.Decrypt.sql";



            if (ExistFunction(functionName.Split('.')[2]))
            {
                DropFunction(functionName.Split('.')[2]);
                CreateFunction(functionName);
            }
            else
            {
                CreateFunction(functionName);
            }

        }
        /// <summary>
        /// Cria função de split no banco de dados
        /// </summary>
        public void CreateSplitStringFunction()
        {
            var functionName = "Krypton.SqlCLR.SplitString.sql";

            if (ExistFunction(functionName.Split('.')[2]))
            {
                DropFunction(functionName.Split('.')[2]);
                CreateFunction(functionName);
            }
            else
            {
                CreateFunction(functionName);
            }

        }

        /// <summary>
        /// Deleta funções que criptografia e descriptografia 
        /// </summary>
        public void DropFunctions()
        {

            var functionsNames = new String[] { "Krypton.SqlCLR.Encrypt.sql", "Krypton.SqlCLR.Decrypt.sql" };

            foreach (var functionName in functionsNames)
            {
                if (ExistFunction(functionName.Split('.')[2]))
                    DropFunction(functionName.Split('.')[2]);
            }
        }

        /// <summary>
        /// Deleta função pelo seu nome passado por parâmetro
        /// </summary>
        /// <param name="functionName">Nome da função</param>
        void DropFunction(string functionName)
        {
            new SqlCommandExecute(this.SqlConnection).ExecuteNonQuery("DROP Function " + functionName);
        }

        /// <summary>
        /// Cria função pelo seu nome passado por parâmetro
        /// </summary>
        /// <param name="functionName">Nome da função</param>
        void CreateFunction(string functionName)
        {
            var kyptonStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(functionName);

            var sqlStream = new StreamReader(kyptonStream).ReadToEnd();

            if (!new SqlCommandExecute(this.SqlConnection).ExecuteNonQuery(sqlStream))
                throw new Exception("Erro ao tentar criar função");
        }

        /// <summary>
        /// Verifica a existencia da função pelo seu nome passado por parâmetro
        /// </summary>
        /// <param name="functionName">Nome da função</param>
        bool ExistFunction(string functionName)
        {

            return new SqlCommandExecute(this.SqlConnection).ExecuteData(string.Format("select * from "+DataBaseName+".sys.objects where [type] in ('FS','TF') and name='{0}'", functionName)).Rows.Count > 0;
        }

        SqlConnection _SqlConnection;

        /// <summary>
        /// Objeto de conexão com o banco de dados
        /// </summary>
        private SqlConnection SqlConnection
        {
            get { return _SqlConnection; }
            set 
            {
                _SqlConnection = value;
                if (value == null)
                    throw new Exception("Não é permitido valor nulo para objeto de conexão");
                if (value.State == System.Data.ConnectionState.Closed)
                    value.Open();

                this.DataBaseName = new SqlConnectionStringBuilder(value.ConnectionString).InitialCatalog;
            }

        }

        /// <summary>
        /// Nome do banco dedados
        /// </summary>
        public String DataBaseName { get; set; }
    }


}
