﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Krypton.SqlCLR
{
    /// <summary>
    /// Provê métodos que executam instruções SQL
    /// </summary>
    public class SqlCommandExecute
    {
        /// <summary>
        /// Construtor que inicializa conexão com o banco de dados
        /// </summary>
        /// <param name="sqlConnection">Objeto de conexão bom o banco de dados</param>
        public SqlCommandExecute(SqlConnection sqlConnection)
        {
            this.SqlConnection = sqlConnection;
        }

        /// <summary>
        /// Executa instruções SQL
        /// </summary>
        /// <param name="sqlCommand">Instrução SQL</param>
        /// <returns>Tabela de dados</returns>
        public DataTable ExecuteData(string sqlCommand)
        {
            var command = SqlConnection.CreateCommand();
            command.CommandText = sqlCommand;
            
            var dataTable = new DataTable();
            dataTable.Load(command.ExecuteReader());

            return dataTable;
        }

        /// <summary>
        /// Executa instruções SQL
        /// </summary>
        /// <param name="sqlCommand">Instrução SQL</param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string sqlCommand)
        {
            var command = SqlConnection.CreateCommand();
            command.CommandText = sqlCommand;


            return command.ExecuteNonQuery()!=0;
        }

        SqlConnection SqlConnection { get; set; }
    }
}
