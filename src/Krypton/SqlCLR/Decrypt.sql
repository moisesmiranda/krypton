﻿create function [Decrypt]
(
 @cipherString nvarchar(4000),
 @useHashing bit,
 @key nvarchar(200),
 @generateException bit
)
returns nvarchar(4000)
AS EXTERNAL NAME Krypton.[Krypton.Krypton].[Decrypt]
