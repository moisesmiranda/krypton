﻿create function Encrypt
(
 @toEncrypt nvarchar(4000),
 @useHashing bit,
 @key nvarchar(200)
)
returns nvarchar(4000)
AS EXTERNAL NAME Krypton.[Krypton.Krypton].Encrypt
