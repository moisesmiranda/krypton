﻿CREATE FUNCTION [dbo].[SplitString](
    @Text VARCHAR(max)
  , @delimit VARCHAR(max) = ','
) RETURNS @result TABLE (item VARCHAR(8000)) 
WITH ENCRYPTION
BEGIN
DECLARE @part VARCHAR(8000)
WHILE CHARINDEX(@delimit,@Text,0) <> 0
BEGIN
SELECT
  @part=RTRIM(LTRIM(
          SUBSTRING(@Text,1,
        CHARINDEX(@delimit,@Text,0)-1))),
  @Text=RTRIM(LTRIM(SUBSTRING(@Text,
          CHARINDEX(@delimit,@Text,0)
        + LEN(@delimit), LEN(@Text))))
IF LEN(@part) > 0
  INSERT INTO @result SELECT @part
END 

IF LEN(@Text) > 0
INSERT INTO @result SELECT @Text
RETURN
END
