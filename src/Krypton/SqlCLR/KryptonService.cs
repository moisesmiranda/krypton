﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data.SqlClient;
using Krypton.SqlCLR;

namespace Krypton.SqlCLR
{
    /// <summary>
    /// Interface de migração do assembly Kryton.dll para o banco de dados
    /// </summary>
    public class KryptonService
    {
        /// <summary>
        /// Inicializa conexão com o banco de dados
        /// </summary>
        /// <param name="sqlConnection">Objeto de conexão com o banco de dados</param>
        /// <param name="checkDependences">True para verificar dependêcias do assembly com as funções de script de criptografia e descriptografia. False a interface executa funções de migração Krypton sem verificar se a mesma está sendo referenciada pelas funções de scriptç</param>
        public KryptonService(SqlConnection sqlConnection, bool checkDependences)
        {
            this.SqlConnection = sqlConnection;
            this.CheckDependeces = checkDependences;
        }

        /// <summary>
        /// Cria as funções de criptografia e descriptografia no banco habilitando a CLR
        /// </summary>
        public void CreateModule()
        {

            EnableCLR();

            if (CheckDependeces)
                new SqlFunctionService(this.SqlConnection).DropFunctions();
            if (ExistsKryptonCLR())
            {
                DropKryptonCLR();
                CreateKryptonCLR();
            }
            else
            {
                CreateKryptonCLR();
            }
        }

        /// <summary>
        /// Cria assembly Krypton.dll no servidor de banco de dados
        /// </summary>
        private void CreateKryptonCLR()
        {
            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();

            var kyptonStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Krypton.SqlCLR.Krypton.sql");

            var sqlStream = new StreamReader(kyptonStream).ReadToEnd();

            new SqlCommandExecute(this.SqlConnection).ExecuteNonQuery(sqlStream);

        }

        /// <summary>
        /// Deleta assembly Krypton.dll do banco de dados
        /// </summary>
        public void DropKryptonCLR()
        {
            new SqlCommandExecute(this.SqlConnection).ExecuteNonQuery("DROP ASSEMBLY Krypton");

        }

        /// <summary>
        /// Verifica a existencia do Assembly Krypton no banco de dados
        /// </summary>
        /// <returns></returns>
        bool ExistsKryptonCLR()
        {
            //var conn = Connection.Instance;

            return new SqlCommandExecute(this.SqlConnection).ExecuteData("select * from sys.assemblies where name = 'Krypton'").Rows.Count > 0;
        }

        /// <summary>
        /// Habilita servidor de banco de dados para CLR
        /// </summary>
        void EnableCLR()
        {
            //var conn = Connection.Instance;
            var cmd = new SqlCommandExecute(this.SqlConnection);
            if (!cmd.ExecuteNonQuery("sp_configure 'CLR Enable', 1"))
                throw new Exception("Não foi possível habilitar CLR");

            cmd.ExecuteNonQuery("RECONFIGURE");
        }

        SqlConnection _SqlConnection;
        private SqlConnection SqlConnection
        {
            get { return _SqlConnection; }
            set { _SqlConnection = value; }
        }

        bool CheckDependeces { get; set; }
    }
}
