﻿namespace Krypton.Demo
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCrypt = new System.Windows.Forms.Button();
            this.cmdGenerateFunction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCrypt
            // 
            this.btnCrypt.Location = new System.Drawing.Point(86, 38);
            this.btnCrypt.Name = "btnCrypt";
            this.btnCrypt.Size = new System.Drawing.Size(136, 23);
            this.btnCrypt.TabIndex = 0;
            this.btnCrypt.Text = "Criptografia";
            this.btnCrypt.UseVisualStyleBackColor = true;
            this.btnCrypt.Click += new System.EventHandler(this.btnCrypt_Click);
            // 
            // cmdGenerateFunction
            // 
            this.cmdGenerateFunction.Location = new System.Drawing.Point(86, 67);
            this.cmdGenerateFunction.Name = "cmdGenerateFunction";
            this.cmdGenerateFunction.Size = new System.Drawing.Size(136, 23);
            this.cmdGenerateFunction.TabIndex = 1;
            this.cmdGenerateFunction.Text = "Gerar funções no banco";
            this.cmdGenerateFunction.UseVisualStyleBackColor = true;
            this.cmdGenerateFunction.Click += new System.EventHandler(this.cmdGenerateFunction_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.cmdGenerateFunction);
            this.Controls.Add(this.btnCrypt);
            this.MaximizeBox = false;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Criptografia";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCrypt;
        private System.Windows.Forms.Button cmdGenerateFunction;
    }
}