﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Krypton.SqlCLR;

namespace Krypton.Demo
{
    public partial class frmGenerationFunctions : Form
    {
        public frmGenerationFunctions()
        {
            InitializeComponent();
        }

        private void cmdGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                var connection = GetConnection();

                new KryptonService(connection, true).CreateModule();

                MessageBox.Show("Operação executada com sucesso","Operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cmdGenerateFunctions_Click(object sender, EventArgs e)
        {
            try
            {
                var connection = GetConnection();
                new SqlFunctionService(connection).CreateFunctions();
                new SqlFunctionService(connection).CreateSplitStringFunction();
                MessageBox.Show("Operação executada com sucesso", "Operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        SqlConnection GetConnection()
        {
            var strSql = new SqlConnectionStringBuilder();
            strSql.DataSource = txtServer.Text;
            strSql.InitialCatalog = txtDataBase.Text;
            strSql.UserID = txtUser.Text;
            strSql.Password = txtPassword.Text;

            var connection = new SqlConnection(strSql.ToString());

            connection.Open();

            return connection;
        }

    }
}
