﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Krypton.Demo
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btnCrypt_Click(object sender, EventArgs e)
        {
            new frmCrypt().Show();
        }

        private void cmdGenerateFunction_Click(object sender, EventArgs e)
        {
            new frmGenerationFunctions().Show();
        }


    }
}
