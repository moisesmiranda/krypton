﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Krypton.SqlCLR
{
    public class SqlCommandExecute
    {
        public SqlCommandExecute(SqlConnection sqlConnection)
        {
            this.SqlConnection = sqlConnection;
        }
        public DataTable ExecuteData(string sqlCommand)
        {
            var command = SqlConnection.CreateCommand();
            command.CommandText = sqlCommand;
            
            var dataTable = new DataTable();
            dataTable.Load(command.ExecuteReader());

            return dataTable;
        }

        public bool ExecuteNonQuery(string sqlCommand)
        {
            var command = SqlConnection.CreateCommand();
            command.CommandText = sqlCommand;


            return command.ExecuteNonQuery()!=0;
        }

        SqlConnection SqlConnection { get; set; }
    }
}
