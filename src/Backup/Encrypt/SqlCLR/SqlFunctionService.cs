﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using Krypton.SqlCLR;
using System.Data.SqlClient;

namespace ConstructionOne.DI.SqlCLR
{
    public class SqlFunctionService
    {
        public SqlFunctionService(SqlConnection sqlConnection)
        {
            this.SqlConnection = sqlConnection;
        }

        public void CreateFunctionEmbeded(string functionName)
        {
            if (ExistFunction(functionName))
            {
                DropFunction(functionName);
                CreateFunction(functionName);
            }
            else
            {
                CreateFunction(functionName);
            }
        }

        void DropFunction(string functionName)
        {
            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            var kyptonStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ConstructionOne.DI.SqlCLR."+functionName+".sql");

            var sqlStream = new StreamReader(kyptonStream).ReadToEnd();
        }

        void CreateFunction(string functionName)
        {
            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            var kyptonStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ConstructionOne.DI.SqlCLR.Krypton.Sql");

            var sqlStream = new StreamReader(kyptonStream).ReadToEnd();
        }

        bool ExistFunction(string functionName)
        {
            return new SqlCommandExecute(this.SqlConnection).ExecuteData("select * from sbo_constone.sys.objects where [type] in ('FN', 'IF', 'TF') where name="+functionName+"Sql").Rows.Count>0;
        }

        SqlConnection _SqlConnection;

        public SqlConnection SqlConnection
        {
            get { return _SqlConnection; }
            set { _SqlConnection = value; }
        }
    }
}
