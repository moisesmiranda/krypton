﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data.SqlClient;
using Krypton.SqlCLR;

namespace ConstructionOne.DI.SqlCLR
{
    public class KryptonLicenceService
    {
        public  void CreateModuleLicence(SqlConnection sqlConnection)
        {
            this.SqlConnection = sqlConnection;
            EnableCLR();

            
        }

       public  void CreateKryptonCLR()
        {
            var kyptonStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ConstructionOne.DI.SqlCLR.Krypton.sql");

           var sqlStream =  new StreamReader(kyptonStream).ReadToEnd();

           //this.SqlConnection.CreateCommand().CommandText = .SqlServerExecute(sqlStream);
           
        }

        bool ExistsKrytonCLR()
        {
            //var conn = Connection.Instance;

            return new SqlCommandExecute(this.SqlConnection).ExecuteData( "select * from sys.assemblies where name = 'Krypton'").Rows.Count > 0;
        }

        void EnableCLR()
        {
            //var conn = Connection.Instance;
            var cmd = new SqlCommandExecute(this.SqlConnection);
            if(! cmd.ExecuteNonQuery("sp_configure 'CLR Enable', 1"))
                throw new Exception("Não foi possível habilitar CLR");

            cmd.ExecuteNonQuery("RECONFIGURE");
        }

        SqlConnection _SqlConnection;

        public SqlConnection SqlConnection
        {
            get { return _SqlConnection; }
            set { _SqlConnection = value; }
        }
        
        
    }
}
