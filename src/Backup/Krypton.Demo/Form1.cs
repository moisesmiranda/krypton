﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Krypton.Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExec_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtKey.Text) || string.IsNullOrEmpty(txtText.Text))
                    throw new Exception("Informe texto e a chave para criptografia");

                txtResultEncrypt.Text = Krypton.Encrypt(txtText.Text, true, txtKey.Text).ToString();
                txtResultDescrypt.Text = Krypton.Decrypt(txtResultEncrypt.Text, true, txtKey.Text).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
